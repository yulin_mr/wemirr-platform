package com.wemirr.framework.security.client.entity;

import com.baomidou.mybatisplus.annotation.EnumValue;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonValue;
import lombok.*;
import org.springframework.security.core.GrantedAuthority;

import java.util.Collection;
import java.util.List;

/**
 * @author Levin
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class SecurityUserDetail {

    private Long userId;
    private String userNo;
    private String principal;
    private String name;
    private Long tenantId;
    private String mobile;

    private List<SecurityDictionary<Long>> departments;
    private String deptName;
    private String nickName;
    private String realName;
    private String companyName;
    private Integer userType;
    private List<TenantType> companyTypes;
    private String image;
    private Collection<? extends GrantedAuthority> authorities;
    /**
     * 权限资源
     */
    private List<UserAuthResource> userAuthResources;


    @Data
    @Builder
    @NoArgsConstructor
    @AllArgsConstructor
    public static class UserAuthResource {
        private Long userId;
        private UserAuthResourceType type;
        private Long resourceId;
        private String resourceName;
    }

    /**
     * @author Levin
     */
    @Getter
    @JsonFormat
    @AllArgsConstructor
    public enum UserAuthResourceType {


        /**
         * 1.系统；2.角色；3.区域；4.客户；5.服务商；6.承运商 7.合同 8.印章 9.项目
         */
        OS(1, "能进的系统"),
        ROLE(2, "角色"),
        REGION(3, "区域"),
        CUSTOMER(4, "客户"),
        SERVICE_PROVIDER(5, "服务商"),
        CAPACITY(6, "承运商"),
        CONTRACT(7, "合同"),
        SEAL(8, "印章"),
        PROJECT(9, "项目"),
        ;
        private final Integer type;
        private final String desc;

        @JsonCreator
        public static UserAuthResourceType of(Integer type) {
            if (type == null) {
                return null;
            }
            for (UserAuthResourceType info : values()) {
                if (info.type.equals(type)) {
                    return info;
                }
            }
            return null;
        }

        public boolean eq(UserAuthResourceType type) {
            if (type == null) {
                return false;
            }
            for (UserAuthResourceType value : values()) {
                if (value == type) {
                    return true;
                }
            }
            return false;
        }
    }


    /**
     * <p>
     * TenantType
     * </p>
     *
     * @author Levin
     * @since 2020-02-14
     */
    @Getter
    @JsonFormat
    @AllArgsConstructor
    public enum TenantType {

        /**
         * (1=壹站 4=运力 3=服务商 2=三方)
         */
        OIT(1, "壹站"),
        /**
         * 三方
         */
        THIRD(2, "三方"),

        SERVICE(3, "服务商"),
        /**
         * 运力
         */
        CAPACITY(4, "运力"),


        ;
        @EnumValue
        @JsonValue
        private final Integer type;
        private final String desc;

        @JsonCreator
        public static TenantType of(Integer type) {
            if (type == null) {
                return null;
            }
            for (TenantType info : values()) {
                if (info.type.equals(type)) {
                    return info;
                }
            }
            return null;
        }

        public boolean eq(String val) {
            return this.name().equalsIgnoreCase(val);
        }

        public boolean eq(TenantType val) {
            if (val == null) {
                return false;
            }
            return eq(val.name());
        }
    }


}
