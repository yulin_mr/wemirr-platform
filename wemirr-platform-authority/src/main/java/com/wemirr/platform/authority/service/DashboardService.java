package com.wemirr.platform.authority.service;

import com.wemirr.platform.authority.domain.vo.DashboardResp;

/**
 * @author Levin
 */
public interface DashboardService {

    DashboardResp dashboard();

}
