package com.wemirr.platform.authority.configuration.integration.primary;

import com.wemirr.platform.authority.configuration.integration.IntegrationAuthentication;
import org.springframework.stereotype.Component;

/**
 * 集成验证码认证
 *
 * @author Levin
 **/
@Component
public class VerificationCodeIntegrationAuthenticator extends UsernamePasswordAuthenticator {

    private final static String VERIFICATION_CODE_AUTH_TYPE = "vc";

    //@Autowired
    //private VerificationCodeClient verificationCodeClient;

    @Override
    public void prepare(IntegrationAuthentication integrationAuthentication) {
        String vcToken = integrationAuthentication.getAuthParameter("vc_token");
        String vcCode = integrationAuthentication.getAuthParameter("vc_code");
        //验证验证码
//        ApiResult result = verificationCodeClient.validate(vcToken, vcCode, null);
//        if (!result.isSuccessful()) {
//            throw new OAuth2Exception("验证码错误");
//        }
    }

    @Override
    public boolean support(IntegrationAuthentication integrationAuthentication) {
        return VERIFICATION_CODE_AUTH_TYPE.equals(integrationAuthentication.getAuthType());
    }
}